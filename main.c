#include <iostm8s003f3.h>
#include <intrinsics.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "uart.h"

#define HX711_DDR PC_DDR
#define HX711_ODR PC_ODR
#define HX711_IDR PC_IDR
#define HX711_CR1 PC_CR1
#define HX711_CR2 PC_CR2
#define HX711_SCK 4
#define HX711_DT 3
#define HX711_DEFAULT_IMPULSE_TIME 50

#define HX711_TIME_TO_READ 1 //hz
#define HX711_LOOP_TIME (490/HX711_TIME_TO_READ)

#define F_CPU 16000000
#define UART_SPEED 9600

#define CALIBRATION_SCALE_NUM_RAW_VAL 3
#define CALIBRATION_SCALE_EACH_MEASURE_CAL 10
#define CALIBRATION_SCALE_MAX_WEIGHT_CAL 100
#define CALIBRATION_SCALE_RAW_MORE_THAN_ZERO_LEVEL_PERCENT 10
#define CALIBRATION_SCALE_RAW_PER_KG 26302
#define CALIBRATION_SCALE_WEIGHT(r,z) ((((r>=z)?(r-z):(0))*1000)/CALIBRATION_SCALE_RAW_PER_KG)

//#define TIM4_UPDATE_FREQ (F_CPU/((2^TIM4_PSCR)*TIM4_ARR))

volatile uint32_t cnt = 0;


void __delay_cycles(uint16_t x)
{
asm("loop:  \n"
          "decw x \n"
          "jrne loop \n"
          " nop \n");
}


int putchar(int i)
{
  uart_tx_byte((char)i); 
  return i;
}

static void sys_cfg(void)
{
  //clock
  CLK_PCKENR1 = 0;
  CLK_PCKENR2 = 0;
  CLK_CKDIVR = 0;
  CLK_ICKR_bit.HSIEN = 1;
  CLK_ECKR_bit.HSEEN = 0;
  while(!CLK_ICKR_bit.HSIRDY);
  CLK_SWR = 0xe1;
  
  //tim4 -> 1mhz
  CLK_PCKENR1 |= 0x10;
  TIM4_SR_bit.UIF = 0;
  TIM4_PSCR = 7;
  TIM4_ARR = 0xff;
  TIM4_IER_bit.UIE = 1;
  __enable_interrupt();
  TIM4_CR1_bit.CEN = 1;
  
  //port hx711
  HX711_DDR |= (1<<HX711_SCK);
  HX711_CR1 |= (1<<HX711_SCK);
  HX711_CR2 |= (1<<HX711_SCK);
  
  HX711_DDR &= ~(1<<HX711_DT);  
  HX711_ODR |= (1<<HX711_SCK);
}

uint32_t hx711_read(void)
{
uint32_t data = 0;
//1. down sck
HX711_ODR &= ~(1<<HX711_SCK);
  __delay_cycles(HX711_DEFAULT_IMPULSE_TIME);
//2. wait for DT goes low
while(HX711_IDR & (1<<HX711_DT));
//3. make 24 sck impulses
for(uint8_t i = 0; i<24;i++)
  {
  HX711_ODR |= (1<<HX711_SCK);
  __delay_cycles(HX711_DEFAULT_IMPULSE_TIME);
  data <<=1;
  HX711_ODR &= ~(1<<HX711_SCK);
  
  if(HX711_IDR & (1<<HX711_DT)) 
     data++;
  
  __delay_cycles(HX711_DEFAULT_IMPULSE_TIME);
  }

HX711_ODR |= (1<<HX711_SCK);
__delay_cycles(HX711_DEFAULT_IMPULSE_TIME);
HX711_ODR &= ~(1<<HX711_SCK);
__delay_cycles(HX711_DEFAULT_IMPULSE_TIME);
HX711_ODR |= (1<<HX711_SCK);
if(data&0x800000)
  data|=0xff000000;
return data;
}

#pragma vector=TIM4_OVR_UIF_vector

__interrupt __root  void TIM4_OVR(void)
{
  cnt++; 
  TIM4_SR_bit.UIF = 0;
}

uint32_t calibrate_scale(uint32_t zero_level)
{
  for(uint8_t i = 0; i!= CALIBRATION_SCALE_NUM_RAW_VAL; i++)
  {
    while(cnt< HX711_LOOP_TIME);
    uint32_t raw = labs(hx711_read());
    if(zero_level)
      if(raw<(zero_level+(((zero_level/100)*CALIBRATION_SCALE_RAW_MORE_THAN_ZERO_LEVEL_PERCENT))))
        {
        zero_level = ((zero_level+raw)/2);
        printf("loop %i - set zero level to: %li \r\n",i,zero_level);      
        }
      else
        {
        printf("raw val greater zero_level more than %i percent",CALIBRATION_SCALE_RAW_MORE_THAN_ZERO_LEVEL_PERCENT);      
        break;
        }
    else
      {
      printf("loop %i - set zero level to: %li \r\n",i,zero_level);      
      zero_level = raw;
      }
    cnt = 0;
  }
  
  return zero_level;
}


int main( void )
{
  sys_cfg();
  uart_init(UART_SPEED,F_CPU);
  printf("hello!\r\n");
  uint32_t zero_level = calibrate_scale(0);
  uint8_t measure_no = 0;
    
  while(1)
  {
    if(cnt> HX711_LOOP_TIME)
    {
    uint32_t raw = labs(hx711_read());
    printf("weight: %li g\r\n",CALIBRATION_SCALE_WEIGHT(raw,zero_level));
    
    if(measure_no==CALIBRATION_SCALE_EACH_MEASURE_CAL)
      {
      if(CALIBRATION_SCALE_WEIGHT(raw,zero_level)<CALIBRATION_SCALE_MAX_WEIGHT_CAL)
         {
         zero_level = calibrate_scale(zero_level);
         }
       measure_no = 0;
      }
    else
         measure_no++;
    cnt = 0;
    }
    
  };
}
